" general settings
set nocompatible
colorscheme industry
syntax on
set encoding=utf8
set laststatus=2
set number
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set background=dark
set ruler
set cursorline
let python_highlight_all = 1

"personal kep map
" F3: Toggle list (display unprintable characters).
nnoremap <F3> :set list!<CR>
" F2 toggle paste
nnoremap <F2> :set paste!<CR>
" F5 delete all trailing whitespaces
" http://vim.wikia.com/wiki/Remove_unwanted_spaces
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>
" F6 toggle line numbers
nnoremap <F6> :set number!<CR>

" buffer navigation
nnoremap <tab> :bnext<CR>
nnoremap <S-tab> :bprevious<CR>

" Tab Setting Reference for Python and Vim
autocmd FileType python set sw=4
autocmd FileType python set ts=4
autocmd FileType python set sts=4
autocmd FileType html set sw=2
autocmd FileType html set ts=2
autocmd FileType html set sts=2
autocmd FileType htmldjango set sw=2
autocmd FileType htmldjango set ts=2
autocmd FileType htmldjango set sts=2
set expandtab
set autoindent

" plugin folder to create
" ~/.vim/pack/foo/start
"
" plugin to install (git clone)
" https://github.com/tpope/vim-fugitive.git

